<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!---banner--->
<div class="banner">
    <div class="container">
        <div class="banner-grids">
            <div class="col-md-6 banner-grid">
                <img src="images/srceen.png" class="img-responsive" alt=""/>
            </div>
            <div class="col-md-6 banner-grid">
                <h3>Professional Information Technology Services</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, nobis provident. Ab ad aliquid
                    aperiam corporis distinctio dolore error ex incidunt minima minus officiis, omnis quibusdam quisquam
                    soluta unde veniam?
                </p>
                <a href="#" class="button">get started</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!---banner--->
<!---brilliantly --->
<div class="content">
    <div class="brilliant-section">
        <div class="container">
            <h2>this is what we do.</h2>
            <h5>We get the job done, no matter the project</h5>
            <div class="brilliant-grids">
                <div class="col-md-4 brilliant-grid">
                    <div class="brilliant-left">
                        <i class="glyphicon glyphicon-cog" aria-hidden="true"></i>
                    </div>
                    <div class="brilliant-right">
                        <h4>Expert Web Design</h4>
                        <p>We strive to deliver the very best possible work that’s available out there, at any time.
                            That’s how we set ourselves apart.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-4 brilliant-grid">
                    <div class="brilliant-left">
                        <i class="glyphicon glyphicon-cloud" aria-hidden="true"></i>
                    </div>
                    <div class="brilliant-right">
                        <h4>ftp services</h4>
                        <p>We strive to deliver the very best possible work that’s available out there, at any time.
                            That’s how we set ourselves apart.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-4 brilliant-grid">
                    <div class="brilliant-left">
                        <i class="glyphicon glyphicon-signal" aria-hidden="true"></i>
                    </div>
                    <div class="brilliant-right">
                        <h4>Support Service</h4>
                        <p>We strive to deliver the very best possible work that’s available out there, at any time.
                            That’s how we set ourselves apart.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="brilliant-grids">
                <div class="col-md-4 brilliant-grid">
                    <div class="brilliant-left">
                        <i class="glyphicon glyphicon-globe" aria-hidden="true"></i>
                    </div>
                    <div class="brilliant-right">
                        <h4>multi domain</h4>
                        <p>We strive to deliver the very best possible work that’s available out there, at any time.
                            That’s how we set ourselves apart.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-4 brilliant-grid">
                    <div class="brilliant-left">
                        <i class="glyphicon glyphicon-link" aria-hidden="true"></i>
                    </div>
                    <div class="brilliant-right">
                        <h4>Link Building</h4>
                        <p>We strive to deliver the very best possible work that’s available out there, at any time.
                            That’s how we set ourselves apart.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-4 brilliant-grid">
                    <div class="brilliant-left">
                        <i class="glyphicon glyphicon-phone" aria-hidden="true"></i>
                    </div>
                    <div class="brilliant-right">
                        <h4>Mobile Optimization</h4>
                        <p>We strive to deliver the very best possible work that’s available out there, at any time.
                            That’s how we set ourselves apart.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!---brilliantly--->
    <!---posts--->
    <div class="post-section">
        <div class="container">
            <h3>Check our recent posts</h3>
            <h5>We like to keep everyone updated</h5>
            <div class="post-grids">
                <div class="col-md-4 post-grid">
                    <a href="single.html" class="mask"><img src="images/p1.jpg" class="img-responsive zoom-img" alt="/"></a>
                    <a href="single.html"><h4>Vestibulum ipsums eros</h4></a>
                    <p>We strive to deliver the very best possible work that’s available out there, at any time. That’s
                        how we set ourselves apart from the competition.</p>
                </div>
                <div class="col-md-4 post-grid">
                    <a href="single.html" class="mask"><img src="images/p2.jpg" class="img-responsive zoom-img" alt="/"></a>
                    <a href="single.html"><h4>Vestibulum ipsums eros</h4></a>
                    <p>We strive to deliver the very best possible work that’s available out there, at any time. That’s
                        how we set ourselves apart from the competition.</p>
                </div>
                <div class="col-md-4 post-grid">
                    <a href="single.html" class="mask"><img src="images/p3.jpg" class="img-responsive zoom-img" alt="/"></a>
                    <a href="single.html"><h4>Vestibulum ipsums eros</h4></a>
                    <p>We strive to deliver the very best possible work that’s available out there, at any time. That’s
                        how we set ourselves apart from the competition.</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!---posts--->
</div>
