<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!---footer--->
<div class="footer-section">
    <div class="container">
        <div class="footer-grids">
            <div class="col-md-3 footer-grid">
                <h4>flickr widget</h4>
                <div class="footer-top">
                    <div class="col-md-4 foot-top">
                        <img src="http://placehold.it/170x170" class="img-responsive" alt=""/>
                    </div>
                    <div class="col-md-4 foot-top">
                        <img src="http://placehold.it/170x170" class="img-responsive" alt=""/>
                    </div>
                    <div class="col-md-4 foot-top">
                        <img src="http://placehold.it/170x170" class="img-responsive" alt=""/>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="footer-top second">
                    <div class="col-md-4 foot-top">
                        <img src="http://placehold.it/170x170" class="img-responsive" alt=""/>
                    </div>
                    <div class="col-md-4 foot-top">
                        <img src="http://placehold.it/170x170" class="img-responsive" alt=""/>
                    </div>
                    <div class="col-md-4 foot-top">
                        <img src="http://placehold.it/170x170" class="img-responsive" alt=""/>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-3 footer-grid">
                <h4>tag cloud</h4>
                <ul>
                    <li><a href="#">Premium</a></li>
                    <li><a href="#">Graphic</a></li>
                    <li><a href="#">1170px</a></li>
                    <li><a href="#">Photoshop Freebie</a></li>
                    <li><a href="#">Designer</a></li>
                    <li><a href="#">Themes</a></li>
                    <li><a href="#">thislooksgreat chris</a></li>
                    <li><a href="#">Lovely Area</a></li>
                    <li><a href="#">You might use it...</a></li>
                </ul>
            </div>
            <div class="col-md-3 footer-grid">
                <h4>recent posts</h4>
                <div class="recent-grids">
                    <div class="col-md-4 recent-great">
                        <img src="http://placehold.it/170x170" class="img-responsive" alt=""/>
                    </div>
                    <div class="col-md-8 recent-great1">
                        <a href="#">This is my lovely headline title for this footer section.</a>
                        <span>22 October 2014</span>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="recent-grids">
                    <div class="col-md-4 recent-great">
                        <img src="http://placehold.it/170x170" class="img-responsive" alt=""/>
                    </div>
                    <div class="col-md-8 recent-great1">
                        <a href="#">This is my lovely headline title for this footer section.</a>
                        <span>22 October 2014</span>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="recent-grids">
                    <div class="col-md-4 recent-great">
                        <img src="http://placehold.it/170x170" class="img-responsive" alt=""/>
                    </div>
                    <div class="col-md-8 recent-great1">
                        <a href="#">This is my lovely headline title for this footer section.</a>
                        <span>22 October 2014</span>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-3 footer-grid">
                <h4>get in touch</h4>
                <p>8901 Marmora Road</p>
                <p>Dodoma, DO4 89GR.</p>
                <p>Telephone : +255 22 222 222</p>
                <p>Telephone : +255 33 333 333</p>
                <p>FAX : + 1 234 567 890</p>
                <p>E-mail : <a href="mailto:example@mail.com"> example@mail.com</a></p>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="copy-section">
            <p>&copy; 2017 IT Company. All rights reserved | Design by <a href="https://about.me/josephmtinangi">JM</a>
            </p>
        </div>
    </div>
</div>
<!---footer--->


</body>
</html>

