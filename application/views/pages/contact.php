<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- //contact -->
<div class="content">
    <div class="contact">
        <div class="container">
            <h2>How To Find Us</h2>
            <div class="contact-bottom">
                <iframe width="600" height="450" frameborder="0" style="border:0"
                        src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJmTQni0KQTRgRvxbk4f4cxeQ&key=AIzaSyCX7oIaPhvXy78qMgMh9Pvf3i6XsuvcveI"
                        allowfullscreen></iframe>
            </div>
            <div class="col-md-4 contact-left">
                <h4>Address</h4>
                <p>est eligendi optio cumque nihil impedit quo minus id quod maxime
                    <span>26 56D Rescue,US</span></p>
                <ul>
                    <li>Free Phone :+1 078 4589 2456</li>
                    <li>Telephone :+1 078 4589 2456</li>
                    <li>Fax :+1 078 4589 2456</li>
                    <li><a href="mailto:info@example.com">info@example.com</a></li>
                </ul>
            </div>
            <div class="col-md-8 contact-left cont">
                <h4>Contact Form</h4>
                <form>
                    <input type="text" value="Name" onfocus="this.value = '';"
                           onblur="if (this.value == '') {this.value = 'Name';}" required="">
                    <input type="email" value="Email" onfocus="this.value = '';"
                           onblur="if (this.value == '') {this.value = 'Email';}" required="">
                    <input type="text" value="Telephone" onfocus="this.value = '';"
                           onblur="if (this.value == '') {this.value = 'Telephone';}" required="">
                    <textarea type="text" onfocus="this.value = '';"
                              onblur="if (this.value == '') {this.value = 'Message...';}"
                              required="">Message...</textarea>
                    <input type="submit" value="Submit">
                    <input type="reset" value="Clear">

                </form>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //contact -->